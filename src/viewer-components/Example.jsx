import {
  Viewer,
  EnabledElement,
  Tool,
  cornerstoneTools,
  Clip,
  Viewport,
} from "./components";
import classes from "./example.module.css";

const images = import.meta.glob("../assets/series-00000/*.dcm", {
  eager: true,
  as: "url",
});
const imageIds = Object.keys(images).map(
  (p) => `dicomweb:${new URL(p, import.meta.url).href}`
);

const firstStack = {
  imageIds,
  currentImageIdIndex: 0,
};

const secondStack = {
  imageIds: [
    `dicomweb:${
      new URL("../assets/series-000002/image-00000.dcm", import.meta.url).href
    }`,
  ],
  currentImageIdIndex: 0,
};

const { StackScrollMouseWheelTool, ZoomMouseWheelTool, PanTool } =
  cornerstoneTools;

export function App() {
  return (
    <div>
      <h2>Cornerstone React Component Example</h2>
      <Viewer>
        <EnabledElement stack={firstStack} stackPrefetch={false}>
          <Tool tool={StackScrollMouseWheelTool} mode="Active" />
          <Clip className={classes.clip} />
          <Viewport className={classes.viewport} />
        </EnabledElement>
        <div style={{ height: 50 }}></div>
        <EnabledElement stack={secondStack}>
          <Tool tool={ZoomMouseWheelTool} mode="Active" />
        </EnabledElement>
        <Tool
          tool={PanTool}
          mode="Active"
          options={{ mouseButtonMask: 1 }}
          interactionTypes={["Mouse"]}
        />
      </Viewer>
    </div>
  );
}
