import {
  Viewer,
  EnabledElement,
  Tool,
  cornerstone,
  cornerstoneTools,
  getToolName,
} from "./components";

const scheme = "wadouri";
const baseUrl =
  "https://tools.cornerstonejs.org/examples/assets/dicom/bellona/";

const firstSeries = ["topogram/IM-0001-0001.dcm"];

const secondSeries = Array.from({ length: 36 }).map(
  (v, i) => `chest_lung/${i + 1}.dcm`
);

const firstStack = {
  currentImageIdIndex: 0,
  imageIds: firstSeries.map(
    (seriesImage) => `${scheme}:${baseUrl}${seriesImage}`
  ),
};

const secondStack = {
  currentImageIdIndex: 0,
  imageIds: secondSeries.map(
    (seriesImage) => `${scheme}:${baseUrl}${seriesImage}`
  ),
};

const synchronizer = new cornerstoneTools.Synchronizer(
  // Cornerstone event that should trigger synchronizer
  "cornerstonenewimage",
  // Logic that should run on target elements when event is observed on source elements
  cornerstoneTools.updateImageSynchronizer
);

const { StackScrollMouseWheelTool, ReferenceLinesTool } = cornerstoneTools;

const { IMAGE_RENDERED } = cornerstone.EVENTS;

const onImageRendered = () => {
  // console.log("IMAGE_RENDERED", event);
};

export function App() {
  return (
    <div>
      <h2>Cornerstone React Component Example</h2>
      <Viewer>
        <div style={{ display: "flex" }}>
          <div style={{ marginRight: 20 }}>
            <EnabledElement
              stack={firstStack}
              synchronizer={synchronizer}
              syncTools={[getToolName(ReferenceLinesTool)]}
            ></EnabledElement>
          </div>
          <div>
            <EnabledElement
              stack={secondStack}
              synchronizer={synchronizer}
              syncTools={[getToolName(ReferenceLinesTool)]}
              stackPrefetch={true}
              listeners={{
                [IMAGE_RENDERED]: onImageRendered,
              }}
            ></EnabledElement>
          </div>
          <Tool tool={StackScrollMouseWheelTool} mode="Active" />
          <Tool
            tool={ReferenceLinesTool}
            mode="Enabled"
            options={{
              mouseButtonMask: 1,
              synchronizationContext: synchronizer,
            }}
          />
        </div>
      </Viewer>
    </div>
  );
}
