import { useEffect, useRef, useState } from "react";
import { EnabledElementContext } from "./context";
import { useViewer } from "../Viewer";
import { cornerstone, cornerstoneTools } from "../cornerstone";

export function EnabledElement({
  children,
  stack,
  stackPrefetch = false,
  width = 512,
  height = 512,
  synchronizer,
  syncTools = [],
  listeners = {},
}) {
  const elementRef = useRef(null);

  const [enabledElement, setEnabledElement] = useState(null);

  const { setElementForViewer, removeElementForViewer } = useViewer();

  useEffect(() => {
    const element = elementRef.current;

    setElementForViewer(element, false);

    const onWindowResize = () => {
      cornerstone.resize(element);
    };

    const enableCornerstone = async () => {
      // Enable the DOM Element for use with Cornerstone
      cornerstone.enable(element);

      // Load the first image in the stack
      const image = await cornerstone.loadAndCacheImage(stack.imageIds[0]);

      // Display the first image
      cornerstone.displayImage(element, image);

      if (synchronizer) {
        synchronizer.add(element);
      }

      // Add the stack tool state to the enabled element
      cornerstoneTools.addStackStateManager(element, ["stack", ...syncTools]);
      cornerstoneTools.addToolState(element, "stack", stack);

      if (stackPrefetch) {
        cornerstoneTools.stackPrefetch.enable(element);
      }

      setEnabledElement(element);

      setElementForViewer(element, true);

      Object.keys(listeners).forEach((event) => {
        element.addEventListener(event, listeners[event]);
      });
      window.addEventListener("resize", onWindowResize);
    };

    const disableCornerstone = () => {
      removeElementForViewer(element);
      Object.keys(listeners).forEach((event) => {
        element.removeEventListener(event, listeners[event]);
      });
      window.removeEventListener("resize", onWindowResize);
      cornerstone.disable(element);
    };

    enableCornerstone();
    return () => {
      disableCornerstone();
    };
  }, [
    stack,
    stackPrefetch,
    synchronizer,
    syncTools,
    listeners,
    setElementForViewer,
    removeElementForViewer,
  ]);

  return (
    <div ref={elementRef} style={{ position: "relative", width, height }}>
      <EnabledElementContext.Provider value={enabledElement}>
        {children}
      </EnabledElementContext.Provider>
    </div>
  );
}
