import { createContext, useContext } from "react";

export const EnabledElementContext = createContext();

export function useEnabledElement() {
  const enabledElement = useContext(EnabledElementContext);
  return enabledElement;
}
