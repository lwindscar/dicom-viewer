import { useState, useEffect } from "react";
import clsx from "clsx";
import { useEnabledElement } from "../EnabledElement";
import { cornerstoneTools, cornerstone } from "../cornerstone";
import classes from "./style.module.css";

export function Clip({ className, style }) {
  const element = useEnabledElement();

  if (!element) {
    return null;
  }

  return (
    <div className={clsx([classes.clip, className])} style={style}>
      <ElementEnabledClip element={element} />
      <ElementEnabledImageId element={element} />
    </div>
  );
}

function ElementEnabledClip({ element }) {
  const [isPlaying, setIsPlaying] = useState(false);

  const onPlay = () => {
    cornerstoneTools.playClip(element, 5);
    setIsPlaying(true);
  };

  const onStop = () => {
    cornerstoneTools.stopClip(element);
    setIsPlaying(false);
  };

  return (
    <>
      <button onClick={onPlay} disabled={isPlaying}>
        Play
      </button>
      <button onClick={onStop} disabled={!isPlaying}>
        Stop
      </button>
    </>
  );
}

const { NEW_IMAGE } = cornerstone.EVENTS;

function ElementEnabledImageId({ element }) {
  const [imageId, setImageId] = useState("");

  useEffect(() => {
    const onNewImage = () => {
      const enabledElement = cornerstone.getEnabledElement(element);
      const { imageId } = enabledElement.image;
      const numId = imageId.slice(
        imageId.lastIndexOf("-") + 1,
        imageId.lastIndexOf(".")
      );
      setImageId(numId);
    };

    element.addEventListener(NEW_IMAGE, onNewImage);
    return () => {
      element.removeEventListener(NEW_IMAGE, onNewImage);
    };
  }, [element]);

  return <div>{imageId}</div>;
}
