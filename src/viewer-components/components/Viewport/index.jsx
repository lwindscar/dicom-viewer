import { useState, useEffect } from "react";
import clsx from "clsx";
import { useEnabledElement } from "../EnabledElement";
import { cornerstone } from "../cornerstone";
import classes from "./style.module.css";

export function Viewport({ className, style }) {
  const element = useEnabledElement();

  if (!element) {
    return null;
  }

  return (
    <div className={clsx([classes.viewport, className])} style={style}>
      <ElementEnabledViewport element={element} />
    </div>
  );
}

const { IMAGE_RENDERED } = cornerstone.EVENTS;

function ElementEnabledViewport({ element }) {
  const [viewport, setViewport] = useState(() =>
    cornerstone.getDefaultViewport(null, undefined)
  );

  useEffect(() => {
    const onImageRendered = () => {
      const viewport = cornerstone.getViewport(element);
      setViewport(viewport);
    };

    element.addEventListener(IMAGE_RENDERED, onImageRendered);
    return () => {
      element.removeEventListener(IMAGE_RENDERED, onImageRendered);
    };
  }, [element]);

  return (
    <>
      <div className={classes.bottomLeft}>Zoom: {viewport.scale}</div>
      <div className={classes.bottomRight}>
        WW/WC: {viewport.voi.windowWidth} / {viewport.voi.windowCenter}
      </div>
    </>
  );
}
