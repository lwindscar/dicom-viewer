import { useEffect, useMemo } from "react";
import { cornerstoneTools } from "../cornerstone";
import { getToolName } from "../utils";

export function CornerstoneToolForElement({
  element,
  tool,
  mode,
  options,
  interactionTypes,
}) {
  const setMode = useMemo(() => {
    if (mode === "Active") {
      return cornerstoneTools.setToolActiveForElement;
    }

    if (mode === "Passive") {
      return cornerstoneTools.setToolPassiveForElement;
    }

    if (mode === "Enabled") {
      return cornerstoneTools.setToolEnabledForElement;
    }

    if (mode === "Disabled") {
      return cornerstoneTools.setToolDisabledForElement;
    }
  }, [mode]);

  const toolName = useMemo(() => getToolName(tool), [tool]);

  useEffect(() => {
    cornerstoneTools.addToolForElement(element, tool);
    return () => {
      cornerstoneTools.removeToolForElement(element, toolName);
    };
  }, [element, tool, toolName]);

  useEffect(() => {
    setMode(element, toolName, options, interactionTypes);
  }, [element, setMode, toolName, options, interactionTypes]);

  return null;
}
