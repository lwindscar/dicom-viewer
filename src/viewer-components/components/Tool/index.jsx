import { CornerstoneTool } from "./CornerstoneTool";
import { CornerstoneToolForElement } from "./CornerstoneToolForElement";
import { useEnabledElement } from "../EnabledElement";

export function Tool({
  tool,
  mode = "Disabled",
  options = {},
  interactionTypes,
}) {
  const element = useEnabledElement();

  if (!tool) {
    return null;
  }

  if (element === undefined) {
    return (
      <CornerstoneTool
        tool={tool}
        mode={mode}
        options={options}
        interactionTypes={interactionTypes}
      />
    );
  }

  if (element != null) {
    return (
      <CornerstoneToolForElement
        element={element}
        tool={tool}
        mode={mode}
        options={options}
        interactionTypes={interactionTypes}
      />
    );
  }
}
