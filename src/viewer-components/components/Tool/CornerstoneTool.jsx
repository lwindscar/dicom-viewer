import { useEffect, useMemo } from "react";
import { cornerstoneTools } from "../cornerstone";
import { getToolName } from "../utils";
import { useAllElementsEnabled } from "../Viewer";

export function CornerstoneTool({ tool, mode, options, interactionTypes }) {
  const isAllElementsEnabled = useAllElementsEnabled();

  if (!isAllElementsEnabled) {
    return null;
  }

  return (
    <AllElementsEnabled
      tool={tool}
      mode={mode}
      options={options}
      interactionTypes={interactionTypes}
    />
  );
}

function AllElementsEnabled({ tool, mode, options, interactionTypes }) {
  const setMode = useMemo(() => {
    if (mode === "Active") {
      return cornerstoneTools.setToolActive;
    }

    if (mode === "Passive") {
      return cornerstoneTools.setToolPassive;
    }

    if (mode === "Enabled") {
      return cornerstoneTools.setToolEnabled;
    }

    if (mode === "Disabled") {
      return cornerstoneTools.setToolDisabled;
    }
  }, [mode]);

  const toolName = useMemo(() => getToolName(tool), [tool]);

  useEffect(() => {
    cornerstoneTools.addTool(tool);
    return () => {
      cornerstoneTools.removeTool(toolName);
    };
  }, [tool, toolName]);

  useEffect(() => {
    setMode(toolName, options, interactionTypes);
  }, [setMode, toolName, options, interactionTypes]);

  return null;
}
