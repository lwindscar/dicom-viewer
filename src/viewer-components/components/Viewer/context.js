import { createContext, useContext } from "react";

export const ViewerContext = createContext(null);

export function useViewer() {
  const viewer = useContext(ViewerContext);
  return viewer;
}

export const AllElementsEnabledContext = createContext(false);

export function useAllElementsEnabled() {
  const isAllElementsEnabled = useContext(AllElementsEnabledContext);
  return isAllElementsEnabled;
}
