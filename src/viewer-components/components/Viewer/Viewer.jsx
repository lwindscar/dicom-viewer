import { useState, useMemo } from "react";
import { ViewerContext, AllElementsEnabledContext } from "./context";

export function Viewer({ children }) {
  const [elementMap, setElementMap] = useState(new Map());

  const viewerContextValue = useMemo(() => {
    const setElementForViewer = (element, enabled = false) => {
      setElementMap((map) => {
        map.set(element, enabled);
        return new Map(map.entries());
      });
    };

    const removeElementForViewer = (element) => {
      setElementMap((map) => {
        map.delete(element);
        return new Map(map.entries());
      });
    };

    return {
      setElementForViewer,
      removeElementForViewer,
    };
  }, []);

  const allElementsEnabledContextValue = useMemo(() => {
    const isAllElementsEnabled =
      elementMap.size > 0 &&
      [...elementMap.values()].every((enabled) => enabled);
    return isAllElementsEnabled;
  }, [elementMap]);

  return (
    <div style={{ position: "relative" }}>
      <ViewerContext.Provider value={viewerContextValue}>
        <AllElementsEnabledContext.Provider
          value={allElementsEnabledContextValue}
        >
          {children}
        </AllElementsEnabledContext.Provider>
      </ViewerContext.Provider>
    </div>
  );
}
