const toolNameCache = new Map();

export function getToolName(tool) {
  if (!toolNameCache.has(tool)) {
    const toolName = tool.name.replace(/\d|Tool/g, "");
    toolNameCache.set(tool, toolName);
  }
  return toolNameCache.get(tool);
}
