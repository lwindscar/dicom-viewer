import { CornerstoneViewer } from "./viewer";

// const imageIds = [
//   `dicomweb:${
//     new URL("../assets/series-00000/image-00000.dcm", import.meta.url).href
//   }`,
//   "dicomweb:https://tools.cornerstonejs.org/examples/assets/dicom/bellona/chest_lung/1.dcm",
//   new URL("../assets/IMG-200353-0001.png", import.meta.url).href,
//   "https://rawgit.com/cornerstonejs/cornerstoneWebImageLoader/master/examples/Renal_Cell_Carcinoma.jpg",
// ];

const images = import.meta.glob("../assets/series-00000/*.dcm", {
  eager: true,
  as: "url",
});
const imageIds = Object.keys(images).map(
  (p) => `dicomweb:${new URL(p, import.meta.url).href}`
);

const stack = {
  imageIds,
  currentImageIdIndex: 0,
};

export function App() {
  return (
    <div>
      <h2>Cornerstone React Component Example</h2>
      <CornerstoneViewer stack={{ ...stack }} />
    </div>
  );
}
