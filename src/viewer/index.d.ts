import type { JSX } from "react";

export const App: () => JSX.Element;
