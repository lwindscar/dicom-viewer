import React, { useState, useRef, useEffect } from "react";
import { cornerstone, cornerstoneTools } from "./cornerstone";
import classes from "./viewer.module.css";

export function CornerstoneViewer(props) {
  // const [stack, setStack] = useState(props.stack);
  const [viewport, setViewport] = useState(() =>
    cornerstone.getDefaultViewport(null, undefined)
  );
  const [imageId, setImageId] = useState(props.stack.imageIds[0]);

  const viewerRef = useRef(null);

  useEffect(() => {
    const onWindowResize = () => {
      cornerstone.resize(viewerRef.current);
    };

    const onImageRendered = () => {
      const viewport = cornerstone.getViewport(viewerRef.current);
      setViewport(viewport);
    };

    const onNewImage = () => {
      const enabledElement = cornerstone.getEnabledElement(viewerRef.current);
      setImageId(enabledElement.image.imageId);
    };

    const enableCornerstone = async () => {
      const element = viewerRef.current;
      if (!element) {
        return;
      }

      // Enable the DOM Element for use with Cornerstone
      cornerstone.enable(element);

      // Load the first image in the stack
      const image = await cornerstone.loadAndCacheImage(
        props.stack.imageIds[0]
      );

      // Display the first image
      cornerstone.displayImage(element, image);

      // Add the stack tool state to the enabled element
      const stack = props.stack;
      cornerstoneTools.addStackStateManager(element, ["stack"]);
      cornerstoneTools.addToolState(element, "stack", stack);
      // cornerstoneTools.stackPrefetch.enable(element);

      // const ZoomMouseWheelTool = cornerstoneTools.ZoomMouseWheelTool;
      // cornerstoneTools.addTool(ZoomMouseWheelTool);
      // cornerstoneTools.setToolActive("ZoomMouseWheel", { mouseButtonMask: 1 });

      // const WwwcTool = cornerstoneTools.WwwcTool;
      // cornerstoneTools.addTool(WwwcTool);
      // cornerstoneTools.setToolActive("Wwwc", { mouseButtonMask: 1 });

      const StackScrollMouseWheelTool =
        cornerstoneTools.StackScrollMouseWheelTool;
      cornerstoneTools.addTool(StackScrollMouseWheelTool);
      cornerstoneTools.setToolActive("StackScrollMouseWheel", {});

      // const PanTool = cornerstoneTools.PanTool;
      // cornerstoneTools.addTool(PanTool);
      // cornerstoneTools.setToolActive("Pan", { mouseButtonMask: 1 });

      // const LengthTool = cornerstoneTools.LengthTool;
      // cornerstoneTools.addTool(LengthTool);
      // cornerstoneTools.setToolActive("Length", { mouseButtonMask: 1 });

      element.addEventListener("cornerstoneimagerendered", onImageRendered);
      element.addEventListener("cornerstonenewimage", onNewImage);
      window.addEventListener("resize", onWindowResize);
    };

    const disableCornerstone = () => {
      const element = viewerRef.current;
      if (!element) {
        return;
      }

      element.removeEventListener("cornerstoneimagerendered", onImageRendered);
      element.removeEventListener("cornerstonenewimage", onNewImage);
      window.removeEventListener("resize", onWindowResize);
      cornerstone.disable(element);
    };

    enableCornerstone();
    return () => {
      disableCornerstone();
    };
  }, [props.stack]);

  const onPlay = () => {
    cornerstoneTools.playClip(viewerRef.current, 5);
  };

  const onStop = () => {
    cornerstoneTools.stopClip(viewerRef.current);
  };

  return (
    <div>
      <div className={classes.toolbar}>
        <button onClick={onPlay}>Play</button>
        <button onClick={onStop}>Stop</button>
      </div>
      <div className={classes.viewer} ref={viewerRef}>
        <canvas className="cornerstone-canvas" />
        <div className={classes.bottomLeft}>Zoom: {viewport.scale}</div>
        <div className={classes.bottomRight}>
          WW/WC: {viewport.voi.windowWidth} / {viewport.voi.windowCenter}
        </div>
      </div>
      <div>{imageId}</div>
    </div>
  );
}

export class CornerstoneElement extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      stack: props.stack,
      viewport: cornerstone.getDefaultViewport(null, undefined),
      imageId: props.stack.imageIds[0],
    };

    this.onImageRendered = this.onImageRendered.bind(this);
    this.onNewImage = this.onNewImage.bind(this);
    this.onWindowResize = this.onWindowResize.bind(this);
  }

  render() {
    return (
      <div>
        <div
          className={classes.viewer}
          ref={(input) => {
            this.element = input;
          }}
        >
          <canvas className="cornerstone-canvas" />
          <div className={classes.bottomLeft}>
            Zoom: {this.state.viewport.scale}
          </div>
          <div className={classes.bottomRight}>
            WW/WC: {this.state.viewport.voi.windowWidth} /{" "}
            {this.state.viewport.voi.windowCenter}
          </div>
        </div>
      </div>
    );
  }

  onWindowResize() {
    // console.log("onWindowResize");
    cornerstone.resize(this.element);
  }

  onImageRendered() {
    const viewport = cornerstone.getViewport(this.element);
    // console.log(viewport);
    this.setState({ viewport });
  }

  onNewImage() {
    const enabledElement = cornerstone.getEnabledElement(this.element);
    this.setState({ imageId: enabledElement.image.imageId });
  }

  async componentDidMount() {
    const element = this.element;

    // Enable the DOM Element for use with Cornerstone
    cornerstone.enable(element);

    // Load the first image in the stack
    const image = await cornerstone.loadImage(this.state.imageId);
    // console.log("image", image);

    // Display the first image
    cornerstone.displayImage(element, image);

    // Add the stack tool state to the enabled element
    const stack = this.props.stack;
    cornerstoneTools.addStackStateManager(element, ["stack"]);
    cornerstoneTools.addToolState(element, "stack", stack);

    // const ZoomMouseWheelTool = cornerstoneTools.ZoomMouseWheelTool;
    // cornerstoneTools.addTool(ZoomMouseWheelTool);
    // cornerstoneTools.setToolActive("ZoomMouseWheel", { mouseButtonMask: 1 });

    // const WwwcTool = cornerstoneTools.WwwcTool;
    // cornerstoneTools.addTool(WwwcTool);
    // cornerstoneTools.setToolActive("Wwwc", { mouseButtonMask: 1 });

    const StackScrollMouseWheelTool =
      cornerstoneTools.StackScrollMouseWheelTool;
    cornerstoneTools.addTool(StackScrollMouseWheelTool);
    cornerstoneTools.setToolActive("StackScrollMouseWheel", {});

    // const PanTool = cornerstoneTools.PanTool;
    // cornerstoneTools.addTool(PanTool);
    // cornerstoneTools.setToolActive("Pan", { mouseButtonMask: 1 });

    // const LengthTool = cornerstoneTools.LengthTool;
    // cornerstoneTools.addTool(LengthTool);
    // cornerstoneTools.setToolActive("Length", { mouseButtonMask: 1 });

    // cornerstoneTools.mouseInput.enable(element);
    // cornerstoneTools.mouseWheelInput.enable(element);
    // cornerstoneTools.wwwc.activate(element, 1); // ww/wc is the default tool for left mouse button
    // cornerstoneTools.pan.activate(element, 2); // pan is the default tool for middle mouse button
    // cornerstoneTools.zoom.activate(element, 4); // zoom is the default tool for right mouse button
    // cornerstoneTools.zoomWheel.activate(element); // zoom is the default tool for middle mouse wheel

    // cornerstoneTools.touchInput.enable(element);
    // cornerstoneTools.panTouchDrag.activate(element);
    // cornerstoneTools.zoomTouchPinch.activate(element);

    element.addEventListener("cornerstoneimagerendered", this.onImageRendered);
    element.addEventListener("cornerstonenewimage", this.onNewImage);
    window.addEventListener("resize", this.onWindowResize);
  }

  componentWillUnmount() {
    const element = this.element;
    element.removeEventListener(
      "cornerstoneimagerendered",
      this.onImageRendered
    );
    element.removeEventListener("cornerstonenewimage", this.onNewImage);
    window.removeEventListener("resize", this.onWindowResize);
    cornerstone.disable(element);
  }

  componentDidUpdate() {
    // const stackData = cornerstoneTools.getToolState(this.element, "stack");
    // const stack = stackData.data[0];
    // stack.currentImageIdIndex = this.state.stack.currentImageIdIndex;
    // stack.imageIds = this.state.stack.imageIds;
    // cornerstoneTools.addToolState(this.element, "stack", stack);
    //const imageId = stack.imageIds[stack.currentImageIdIndex];
    //cornerstoneTools.scrollToIndex(this.element, stack.currentImageIdIndex);
  }
}
