import { useRef, useEffect } from "react";

// Load the rendering pieces we want to use (for both WebGL and WebGPU)
import "@kitware/vtk.js/Rendering/Profiles/Geometry";
import "@kitware/vtk.js/Rendering/Profiles/Volume";

// Force DataAccessHelper to have access to various data source
import "@kitware/vtk.js/IO/Core/DataAccessHelper/HtmlDataAccessHelper";
import "@kitware/vtk.js/IO/Core/DataAccessHelper/HttpDataAccessHelper";
import "@kitware/vtk.js/IO/Core/DataAccessHelper/JSZipDataAccessHelper";

import vtkFullScreenRenderWindow from "@kitware/vtk.js/Rendering/Misc/FullScreenRenderWindow";
import vtkRenderWindow from "@kitware/vtk.js/Rendering/Core/RenderWindow";
import vtkRenderer from "@kitware/vtk.js/Rendering/Core/Renderer";
import vtkHttpDataSetReader from "@kitware/vtk.js/IO/Core/HttpDataSetReader";
import vtkVolume from "@kitware/vtk.js/Rendering/Core/Volume";
import vtkVolumeMapper from "@kitware/vtk.js/Rendering/Core/VolumeMapper";

interface Context {
  fullScreenRenderer: vtkFullScreenRenderWindow;
  renderWindow: vtkRenderWindow;
  renderer: vtkRenderer;
  actor: vtkVolume;
  mapper: vtkVolumeMapper;
  reader: vtkHttpDataSetReader;
}

export function App() {
  const vtkContainerRef = useRef<HTMLDivElement>(null);
  const context = useRef<Context>();

  useEffect(() => {
    if (!context.current && vtkContainerRef.current) {
      const fullScreenRenderer = vtkFullScreenRenderWindow.newInstance({
        // @ts-expect-error ts(2345)
        rootContainer: vtkContainerRef.current,
        background: [0.3, 0.3, 0.3],
      });

      const actor = vtkVolume.newInstance();
      const mapper = vtkVolumeMapper.newInstance({
        sampleDistance: 1.1,
      });

      actor.setMapper(mapper);

      const renderer = fullScreenRenderer.getRenderer();
      const renderWindow = fullScreenRenderer.getRenderWindow();

      const reader = vtkHttpDataSetReader.newInstance({
        fetchGzip: true,
      });

      mapper.setInputConnection(reader.getOutputPort());
      mapper.setBlendModeToMaximumIntensity();
      renderer.getActiveCamera().setViewUp(0, 1, 0);

      async function loadFile() {
        await reader.setUrl(
          "https://kitware.github.io/vtk-js/data/volume/LIDC2.vti"
        );
        await reader.loadData();

        const imageData = reader.getOutputData();
        const dataArray = imageData.getPointData().getScalars();
        const rgbTransferFunction = actor
          .getProperty()
          .getRGBTransferFunction(0);
        rgbTransferFunction.setRange(
          ...(dataArray.getRange() as [number, number])
        );

        renderer.addVolume(actor);
        renderer.resetCamera();
        renderWindow.render();
      }

      loadFile();

      context.current = {
        fullScreenRenderer,
        renderWindow,
        renderer,
        actor,
        mapper,
        reader,
      };
    }

    return () => {
      if (context.current) {
        const { fullScreenRenderer, actor, mapper, reader } = context.current;
        actor.delete();
        mapper.delete();
        fullScreenRenderer.delete();
        reader.delete();
        context.current = undefined;
      }
    };
  }, [vtkContainerRef]);

  return <div ref={vtkContainerRef} />;
}
