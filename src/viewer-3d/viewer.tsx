import { useRef, useEffect } from "react";
import {
  RenderingEngine,
  StackViewport,
  Enums,
  // Types,
} from "@cornerstonejs/core";
import * as cornerstoneTools from "@cornerstonejs/tools";
import {
  StackScrollMouseWheelTool,
  ToolGroupManager,
  state,
  // Enums as csToolsEnums,
  Types as csToolsTypes,
} from "@cornerstonejs/tools";
import { init } from "./cornerstone";
import classes from "./viewer.module.css";

const { ViewportType } = Enums;

interface Props {
  stack: {
    imageIds: string[];
    currentImageIdIndex: number;
  };
}

export function CornerstoneViewer(props: Props) {
  // const [viewport, setViewport] = useState({
  //   scale: 0,
  //   voi: {
  //     windowWidth: 0,
  //     windowCenter: 0,
  //   },
  // });
  // const [imageId, setImageId] = useState(props.stack.imageIds[0]);

  const viewerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const enableCornerstone = async () => {
      const element = viewerRef.current;
      if (!element) {
        return;
      }

      await init();

      // Add tools to Cornerstone3D
      // cornerstoneTools.addTool(PanTool);
      // cornerstoneTools.addTool(ZoomTool);
      const toolName = StackScrollMouseWheelTool.toolName;
      const toolAlreadyAdded = state.tools[toolName] !== undefined;
      if (!toolAlreadyAdded) {
        cornerstoneTools.addTool(StackScrollMouseWheelTool);
        // Define a tool group, which defines how mouse events map to tool commands for
        // Any viewport using the group
        const toolGroupId = "STACK_TOOL_GROUP_ID";
        const toolGroup = ToolGroupManager.createToolGroup(
          toolGroupId
        ) as csToolsTypes.IToolGroup;

        // Add tools to the tool group
        toolGroup.addTool(toolName, { loop: true });

        toolGroup.setToolActive(toolName);

        const renderingEngineId = "myRenderingEngine";
        const renderingEngine = new RenderingEngine(renderingEngineId);

        const viewportId = "CT_AXIAL_STACK";
        const viewportInput = {
          viewportId,
          element,
          type: ViewportType.STACK,
        };
        renderingEngine.enableElement(viewportInput);

        toolGroup.addViewport(viewportId, renderingEngineId);

        const { imageIds, currentImageIdIndex } = props.stack;

        const viewport = renderingEngine.getViewport(
          viewportId
        ) as StackViewport;
        viewport.setStack(imageIds, currentImageIdIndex);
        viewport.render();
      }
    };

    // const disableCornerstone = () => {
    //   renderingEngine.disableElement(element);
    //   renderingEngine.destroy();
    // }

    enableCornerstone();
  }, [props.stack]);

  return (
    <div>
      <div className={classes.viewer} ref={viewerRef}>
        {/* <div className={classes.bottomLeft}>Zoom: {viewport.scale}</div>
        <div className={classes.bottomRight}>
          WW/WC: {viewport.voi.windowWidth} / {viewport.voi.windowCenter}
        </div> */}
      </div>
      {/* <div>{imageId}</div> */}
    </div>
  );
}
