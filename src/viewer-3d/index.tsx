import { CornerstoneViewer } from "./viewer";

const images = import.meta.glob("../assets/series-00000/*.dcm", {
  eager: true,
  as: "url",
});
const imageIds = Object.keys(images).map(
  (p) => `dicomweb:${new URL(p, import.meta.url).href}`
);

const stack = {
  imageIds,
  currentImageIdIndex: 0,
};

export function App() {
  return (
    <div>
      <h2>Cornerstone React Component Example</h2>
      <CornerstoneViewer stack={{ ...stack }} />
    </div>
  );
}
